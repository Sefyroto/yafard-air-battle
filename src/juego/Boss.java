package juego;

import java.util.Random;

import Core.Field;
import Core.Sprite;
/**
 * 
 * @author Santiago Lopez Roura
 *
 */
public class Boss extends Enemigo {
	
	int coldown = 0;
	
	/**
	 * Constructor de Boss
	 * @param name
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @param angle
	 * @param path
	 * @param f
	 * @param hp
	 * @param velocidad
	 */
	public Boss(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f, int hp,
			int velocidad) {
		super(name, x1, y1, x2, y2, angle, path, f, 100, 0);
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Disparar crea los proyectiles, con su propia hitbox y su propia velocidad
	 */
	public void disparar() {
		ProyectilEnemigo balaI = new ProyectilEnemigo("BalaI", (this.x1 + this.x2) / 2 - 80,
				(this.y1 + this.y2) / 2 + 0, (this.x1 + this.x2) / 2 - 30, (this.y1 + this.y2) / 2 + 60, 180,
				"imagenes/balaarribaderecha.png", this.f);
		ProyectilEnemigo balaD = new ProyectilEnemigo("BalaD", (this.x1 + this.x2) / 2 + 30,
				(this.y1 + this.y2) / 2 + 0, (this.x1 + this.x2) / 2 + 80, (this.y1 + this.y2) / 2 + 60, 180,
				"imagenes/balaarribaizquierda.png", this.f);

		ProyectilEnemigo balaI2 = new ProyectilEnemigo("BalaI", (this.x1 + this.x2) / 2 - 220,
				(this.y1 + this.y2) / 2 + 0, (this.x1 + this.x2) / 2 - 150, (this.y1 + this.y2) / 2 + 60, 180,
				"imagenes/balaarribaderecha.png", this.f);
		ProyectilEnemigo balaD2 = new ProyectilEnemigo("BalaD", (this.x1 + this.x2) / 2 + 150,
				(this.y1 + this.y2) / 2 + 0, (this.x1 + this.x2) / 2 + 220, (this.y1 + this.y2) / 2 + 60, 180,
				"imagenes/balaarribaizquierda.png", this.f);

	}

	@Override
	public void destruir() {
		if (this.hp == 75) {// Cuando la vida es igual a 75 empieza a moverse 
			this.velocidad = this.velocidad + 5;
			this.setVelocity(-this.velocidad, 0);
		}
		if (this.hp > 1) {
			hp--;
		} else {
			UI.instance.puntos.addPuntos(1000000);
			this.delete();
			MainGame.bossmuerto = true; // si la vida del Boss es 0 muere
		}

	}

	
	@Override
	public void onCollisionEnter(Sprite sprite) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onCollisionExit(Sprite sprite) {
		// TODO Auto-generated method stub

	}

	@Override
	public void update() {
		// Coldown de los disparos del enemigo
		coldown++;
		if (coldown == 100) {
			disparar();
			coldown = 0;
		}
		//mueve al bombardero de izquierda a derecha
		if (this.x2 >= 1920) {
			this.setVelocity(-this.velocidad, 0);
		} else if (this.x1 <= 0) {
			this.setVelocity(this.velocidad, 0);
		}

	}
}
