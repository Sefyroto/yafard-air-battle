package juego;

import Core.Field;
import Core.Sprite;
/**
 * 
 * @author Santiago Lopez Roura
 *
 */
public class EnemigoBasico extends Enemigo {
	
	/**
	 * Creacion del constructor
	 * @param name
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @param angle
	 * @param path
	 * @param f
	 */
	public EnemigoBasico(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f) {
		super(name, x1, y1, x2, y2, angle, path, f, 1, 8);
		this.setVelocity(0, this.velocidad);
	}

	@Override
	public void onCollisionEnter(Sprite sprite) {
		if (sprite instanceof Avion) {
			((Avion) sprite).destruir();
			this.delete();
		}
	}


	@Override
	public void onCollisionExit(Sprite sprite) {

	}

	@Override
	public void destruir() {
		UI.instance.puntos.addPuntos(100);
		this.delete();
	}

}
