package juego;

import java.awt.Font;

import Core.Field;
import Core.Sprite;
/**
 * 
 * @author Santiago Lopez Roura
 *
 */
public class Vidas extends Sprite {

	/**
	 * Constructor de vida
	 * @param x1
	 * @param y1
	 * @param f
	 */
	public Vidas(int x1, int y1, Field f) {
		super("Vida", x1, y1, x1, y1, 0, " ", f);
		this.solid = false;
		this.text = true;
		this.font = new Font("Arial", font.BOLD, 30);
		this.textColor = 0xFFFFFF;
		vida();
	}
	/**
	 * La vida se visualiza en la UI (hud)
	 */
	public void vida() {
		this.path = "Vidas: "+ Avion.instance.hp;
		
	}

}
