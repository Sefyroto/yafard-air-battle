package juego;

import Core.Field;
import Core.Sprite;
/**
 * 
 * @author Santiago Lopez Roura
 *
 */
public class UI {

	public Puntuacion puntos;
	public Vidas vida;
	public static UI instance = null; // instance es una variable, UI es la clase de la variable e instance es el
										// nombre de la variable
	/**
	 * Constructor de la UI
	 * @param x1
	 * @param y1
	 * @param f
	 */
	private UI(int x1, int y1, Field f) {
		this.puntos = new Puntuacion(x1, y1, f);
		this.vida = new Vidas(x1, y1+50, f);
	}
	/**
	 * Instancia la UI
	 * @param x1
	 * @param y1
	 * @param f
	 * @return
	 */
	public static UI getInstance(int x1, int y1, Field f) {
		if (instance == null) {
			instance = new UI(x1, y1, f);
		}
		return instance;
	}
	
}
