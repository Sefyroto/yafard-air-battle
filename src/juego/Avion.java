package juego;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;
/**
 * 
 * @author Santiago Lopez Roura
 *
 */
public class Avion extends PhysicBody {
	
	int hp;
	int velocidad;

	// inicializacion de variables 
	public static Avion instance=null;
	
	/**
	 * Constructor de avion (Protagonista)
	 * @param name
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @param angle
	 * @param path
	 * @param f
	 * @param hp
	 */
	private Avion(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f, int hp) {
		super(name, x1, y1, x2, y2, angle, path, f);
		this.hp=hp;
		this.velocidad=20;
	}
	/**
	 * Comprobacion si existe o no un avion
	 * @param f
	 * @return
	 */
	public static Avion getInstance(Field f) {
		if (instance == null) {
			instance  = new Avion("Nave", 900, 900, 1000, 1000, 0, "imagenes/avionA.png", f, 3); //si no existe avion crea uno nuevo
		}
		return instance; //devuelve el avion creado
	}
	
	
	@Override
	public void onCollisionEnter(Sprite sprite) {

	}

	@Override
	public void onCollisionExit(Sprite sprite) {

	}
	
	/**
	 * Mueve el personaje
	 * @param in
	 */
	public void movimiento(Input in) { 
		if (in == Input.DERECHA) {
			this.setVelocity(this.velocidad, this.velocity[1]);
		} else if (in == Input.IZQUIERDA) {
			this.setVelocity(-this.velocidad, this.velocity[1]);
		} else {
			this.setVelocity(0, this.velocity[1]);
		}
	}
	
	/**
	 * Funcion que reduce la vida al personaje y lo borra cuando ya no tiene
	 */
	public void destruir() {
		if (this.hp>1) {
			this.hp--;
			UI.instance.vida.vida();//Actualiza la vida en en el hud
		}else {
			this.hp--;
			UI.instance.vida.vida();//Actualiza la vida en en el hud
			this.delete();
			
		}

	}
	/**
	 * Es la creacion de un proyectil en la ubicacion del avion con su propia hitbox y velocidad
	 */
	public void disparo(){
		Proyectiles bala = new Proyectiles("Bala", (this.x1+this.x2)/2-10, (this.y1+this.y2)/2-40, (this.x1+this.x2)/2+10, (this.y1+this.y2)/2-20, 180, "imagenes/balaenemigo.png", this.f);
	}


}
