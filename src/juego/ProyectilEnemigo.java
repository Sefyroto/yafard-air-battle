package juego;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;
/**
 * 
 * @author Santiago Lopez Roura
 *
 */
public class ProyectilEnemigo extends PhysicBody {
	/**
	 * Constructor del proyectil
	 * @param name
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @param angle
	 * @param path
	 * @param f
	 */
	public ProyectilEnemigo(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f) {
		super(name, x1, y1, x2, y2, angle, path, f);
		this.setVelocity(0, 11);
		this.trigger = true;
	}

	@Override
	public void onCollisionEnter(Sprite sprite) {
		if (sprite instanceof Avion) {
			this.delete();
			((Avion) sprite).destruir();
		}
	}

	@Override
	public void onCollisionExit(Sprite sprite) {
		// TODO Auto-generated method stub

	}
	@Override
	public void update() {
	    if(this.y1>1000){// si el proyectil se borra al salir de la pantalla 
	        this.delete();
	    }
	}

}
