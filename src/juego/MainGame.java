package juego;

import Core.Field;
import Core.Window;

/**
 * 
 * @author Santiago Lopez Roura
 *
 */

public class MainGame {
	
	static Field f = new Field();
	static Window w = new Window(f);
	// El protagonista es un Avion singelton que se llama para instanciarlo
	static Avion protagonista = Avion.getInstance(f);
	// Es una variable que nos permite saber si el juego finaliza porque el boss a
	// muerto
	public static boolean bossmuerto = false;

	/**
	 * Instanciacion de nivel, de hud e inicio de bucle para saber si el juego pasa de nivel
	 * @param args
	 * @throws InterruptedException
	 */
	public static void main(String[] args) throws InterruptedException {
		// Sirve para mirar en que nivel nos encontramos
		int nivel = 1;

		// Se instancia el hud del juego
		UI hud = UI.getInstance(50, 900, f);

		boolean finjuego = false; // inicializamos una variable para comprobar si el juego a acabado o sigue
		while (finjuego != true) {
			inicializar(nivel);
			juego(nivel);
			if (protagonista.hp > 1 && nivel < 2) { // Comprobamos si se puede subir de nivel y el protagonista esta
													// vivo pasara al siguiente nivel
				nivel++;
			} else {
				finjuego = true; // Comprobamos si el protagonista muere o estamos en el ultimo nivel juego
			}
			Spawner.parartimers(); // Paramos los spawns del primer nivel para poder iniciar el del nivel 2
		}

	}
	/**
	 * Sirve para iniciar el juego
	 * @param nivel
	 * @throws InterruptedException
	 */
	private static void juego(int nivel) throws InterruptedException {

		boolean finnivel = false; // Se instancia una variable para comprobar si el final del nivel acaba

		while (finnivel != true) {

			f.draw();
			movimiento(); // Llama a la funcion
			Thread.sleep(30);
			finnivel = comprobar(nivel); // Compruebo si el nivel a finalizado

		}

	}

	/**
	 * Inicializa el nivel
	 * @param nivel
	 * 
	 */
	private static void inicializar(int nivel) { 
		if (nivel == 1) {
			f.background = "imagenes/fondo.jpg"; //Fondo del primer nivel
			Spawner spawn = new Spawner(f, 1920, 1);//Spawn de enemigos del primer nivel
		} else if (nivel == 2) {
			f.background = "imagenes/fondo2.png";//Fondo del segundo nivel
			Spawner spawn = new Spawner(f, 1920, 2);//Spawn de enemigos del segundo nivel
			Boss boss = new Boss("Boss", 750, 0, 1200, 300, 180, "imagenes/avionBoss.png", f, 100, 0);//Boss que solo aparece en el segundo nuvel
			BombarderoPesado pesado = new BombarderoPesado("BPesado", 810, 150, 1110, 330, 180,	"imagenes/bombardero.png", f);//Bombardero statico(que no se mueve) para hacer de escudo al boss

		}

	}
	/**
	 * Comprobacion de requisitos para poder pasar de nivel
	 * @param nivel
	 * @return
	 */
	private static boolean comprobar(int nivel) {
		if (protagonista.hp < 1 || (nivel == 1 && UI.instance.puntos.puntos > 5000) || bossmuerto == true) {
			return true;//Comprobacion si el protagonista esta vivo, si en el nivel 1 tiene mas de 5000 puntos y si el boss a muerto
		} else {
			return false;
		}
	}
	/**
	 * Movimientos de el avion protagonista
	 */
	private static void movimiento() {

		if (w.getPressedKeys().contains('a')) {
			protagonista.movimiento(Input.IZQUIERDA);
		} else if (w.getPressedKeys().contains('d')) {
			protagonista.movimiento(Input.DERECHA);
		} else {
			protagonista.movimiento(Input.STOP);
		}

		if (w.getKeysDown().contains(' ')) {
			protagonista.disparo();
		}
	}

}
