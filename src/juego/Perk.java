package juego;

import java.util.Random;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;
/**
 * 
 * @author Santiago Lopez Roura
 *
 */
public class Perk extends PhysicBody {
	
	static Random r = new Random();
	/**
	 * Constructor de la perk
	 * @param x1
	 * @param y1
	 * @param path
	 * @param f
	 */
	public Perk(int x1, int y1, String path, Field f) {
		super("perk", x1, y1, x1+50, y1+50, 0, path, f);
		this.setVelocity(0, 5);
		this.trigger = true;
	}

	@Override
	public void onCollisionEnter(Sprite sprite) {
		if (sprite instanceof Avion) {
			habilidad();
			
			this.delete();
		}

	}
	
	/**
	 * da al protagonista un buffo aleatorio 
	 */
	public void habilidad() {

		int x = r.nextInt(3+1);
		if (x==1) {//si es 1 suma 5 a la velocidad del protagonista
			Avion.instance.velocidad = Avion.instance.velocidad + 5;
		}else if (x==2) {//si es 2 suma 1 vida
			Avion.instance.hp++;
			UI.instance.vida.vida();
		}else if (x==3) {//Si es 3 los proyectiles subiran mas rapido
			Proyectiles.velocidad = Proyectiles.velocidad - 5;
		}
		
		
		
		
	}

	@Override
	public void onCollisionExit(Sprite sprite) {
		// TODO Auto-generated method stub

	}

}
