package juego;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import Core.Field;
/**
 * 
 * @author Santiago Lopez Roura
 *
 */
public class Spawner {

	static Timer timer = new Timer();
	static Random r = new Random();
	static Field f;
	static int x;
	static TimerTask task1;
	static TimerTask task2;
	static TimerTask SpawnPesado;
	
	/**
	 * Constructor del spawn
	 * @param f
	 * @param x
	 * @param nivel
	 */
	public Spawner(Field f, int x, int nivel) {
		Spawner.f = f;
		Spawner.x = x; //La x indica la x de las cordenadas
		iniciodespawner(nivel);
		
	}
	
	/**
	 * Genera enemigos aleatoriamente
	 */
	public static void GenerarEnemigo() {
		int x1 = r.nextInt(x - 300) + 150;
		int ra = r.nextInt(10);
		if (ra < 5) {//si el numero aleatorio es menor de 5 spawn de basico
			EnemigoBasico basico = new EnemigoBasico("Basico", x1, 0, x1 + 100, 80, 180, "imagenes/avionM.png", f);
		} else if (ra >= 5 && ra <= 7) {//si el numero aleatorio esta entr 5 y 7 spawn de Acorazado
			Enemigoblindado armor = new Enemigoblindado("Acorazado", x1, 0, x1 + 100, 120, 180, "imagenes/caza.png", f);
		} else if (ra == 8) {//si el numero aleatorio es 8 spawn de verde
			Bombardero aminorar = new Bombardero("Verde", x1, 0, x1 + 100, 120, 180, "imagenes/AvionV.png", f, 5);
		} else {// Si no es ninguno de los anteriores spawn de perk
			//System.out.println("Perk");
			Perk bufo = new Perk(x1, 0, "imagenes/perk.png", f);
		}

	}
	/**
	 * Inicia un Spawn segun el nivel
	 * @param nivel
	 */
	public static void iniciodespawner(int nivel){
		if (nivel == 1) {
			
			task1 = new TimerTask() {//genera enemigos
				@Override
				public void run() {
					GenerarEnemigo();
				}
			};

			task2 = new TimerTask() {//cada x tiempo le añade + 5 a su velocidad base
				@Override
				public void run() {
					Enemigo.extravelocidad+=5;
				}
			};
			/**
			 * Crea un bombardero pesado cada x tiempo
			 */
			SpawnPesado = new TimerTask() {
				@Override
				public void run() {
					BombarderoPesado pesado = new BombarderoPesado("BPesado", 810, 0, 1110, 180, 180, "imagenes/bombardero.png", f);
					if (r.nextInt(10+1) % 2 == 0) {
						pesado.setVelocity(pesado.velocidad, 0);
					}else {
						pesado.setVelocity(-pesado.velocidad, 0);
					}
				}
			};
			timer.schedule(task1, 2000, 1000);
			timer.schedule(task2, 30000, 30000);
			timer.schedule(SpawnPesado, 10000, 30000);
		}else if (nivel == 2) {
			/**
			 * Crea un bombardero pesado cada x tiempo
			 */
			SpawnPesado = new TimerTask() {
				@Override
				public void run() {
					BombarderoPesado pesado = new BombarderoPesado("BPesado", 810, 150, 1110, 330, 180, "imagenes/bombardero.png", f);
					if (r.nextInt(10+1) % 2 == 0) {
						pesado.setVelocity(pesado.velocidad, 0);
					}else {
						pesado.setVelocity(-pesado.velocidad, 0);
					}
				}
			};
			timer.schedule(SpawnPesado, 0, 8000);
		}
	}
	
	/**
	 * Cancela los spawns 
	 */
	public static void parartimers() {
		task1.cancel();
		task2.cancel();
		SpawnPesado.cancel();
	}
}
