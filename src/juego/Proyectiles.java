package juego;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;
/**
 * 
 * @author Santiago Lopez Roura
 *
 */
public class Proyectiles extends PhysicBody {
	public static int velocidad=-10;
	/**
	 * Constructor de proyectiles de avion
	 * @param name
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @param angle
	 * @param path
	 * @param f
	 */
	public Proyectiles(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f) {
		super(name, x1, y1, x2, y2, angle, path, f);
		this.setVelocity(0, velocidad);
		this.trigger = true;
	}
	

	@Override
	public void onTriggerEnter(Sprite sprite) {
		if (sprite instanceof Enemigo) {
			this.delete();
			((Enemigo) sprite).destruir();
		}
		
	}
	
	@Override
	public void onCollisionEnter(Sprite sprite) {
		
	}

	@Override
	public void onCollisionExit(Sprite sprite) {
		

	}
	
	@Override
	public void update() {
	    if(this.y1<0){ //se borra el proyectil si sale de la pantalla
	        this.delete();
	    }
	}


}
