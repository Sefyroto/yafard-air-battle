package juego;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;

public abstract class Enemigo extends PhysicBody {
	
	//agrega extra velocidad
	public static int extravelocidad=0;
	
	int hp;
	int velocidad;
	
	public Enemigo(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f, int hp, int velocidad) {
		super(name, x1, y1, x2, y2, angle, path, f);
		this.hp=hp;
		this.velocidad=velocidad+extravelocidad; //la velocidad extra se suma a la velocidad
		this.trigger=true;
	}
	
	@Override
	public void update() {
	    if(this.y1>1000){
	        this.delete();
	    }
	}
	
	/**
	 * Se llama a la funcion destruir la cual resta vida a el enemigo hasta destruirlo y da puntos para el hud
	 */
	public abstract void destruir();
}
