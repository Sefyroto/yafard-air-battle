package juego;

import Core.Field;
import Core.Sprite;
/**
 * 
 * @author Santiago Lopez Roura
 *
 */
public class Bombardero extends Enemigo {
	/**
	 * Es el Constructor del bombardero
	 * @param name
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @param angle
	 * @param path
	 * @param f
	 * @param hp
	 */
	public Bombardero(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f, int hp) {
		super(name, x1, y1, x2, y2, angle, path, f, hp, 12);
		this.setVelocity(0, this.velocidad);
	}

	@Override
	public void destruir() {
		// cada vez que recibe daño aminora la velocidad hasta morir
		if (this.hp == 5) { 
			this.addVelocity(0, -3);
			hp--;
		}else if (this.hp == 4) {
			this.addVelocity(0, -3);
			hp--;
		}else if (this.hp == 3) {
			this.addVelocity(0, -3);
			hp--;
		}else if (this.hp == 2) {
			this.addVelocity(0, -3);
			hp--;
		}else if (this.hp == 1) {
			UI.instance.puntos.addPuntos(500);	
			this.delete();
		}
	}
	@Override
	public void onCollisionEnter(Sprite sprite) {
		//Cuando entra en colision el bombardero con el avion se aplica la funcion destruir
		if (sprite instanceof Avion) {
			this.delete();
			((Avion) sprite).destruir();
		}
	}

	@Override
	public void onCollisionExit(Sprite sprite) {

	}

}
