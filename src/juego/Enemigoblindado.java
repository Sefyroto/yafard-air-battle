package juego;

import Core.Field;
import Core.Sprite;
/**
 * 
 * @author Santiago Lopez Roura
 *
 */
public class Enemigoblindado extends Enemigo {
	/**
	 * Constructor de Enemigo Blindado
	 * @param name
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @param angle
	 * @param path
	 * @param f
	 */
	public Enemigoblindado(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f) {
		super(name, x1, y1, x2, y2, angle, path, f, 2, 6);
		this.setVelocity(0, this.velocidad);
	}

	@Override
	public void destruir() {
		if (this.hp==2) {
			this.hp--;
			this.addVelocity(0, this.velocidad*2);// Cuando se le quita un hp duplica su velocidad
		}else if (this.hp==1) {
			UI.instance.puntos.addPuntos(150);
			this.delete();
		}
	}

	@Override
	public void onCollisionEnter(Sprite sprite) {
		if (sprite instanceof Avion) {
			this.delete();//Borra el avion enemigo si impacta con el protagonista
			((Avion) sprite).destruir();
		}
	}

	@Override
	public void onCollisionExit(Sprite sprite) {
		// TODO Auto-generated method stub

	}

}
