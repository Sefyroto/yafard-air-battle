package juego;
/**
 * Direcion a la que se dirige el avion
 * @author Santiago Lopez Roura
 *
 */
public enum Input {

	DERECHA,IZQUIERDA,STOP
}
