package juego;

import java.awt.Font;

import Core.Field;
import Core.Sprite;
/**
 * 
 * @author Santiago Lopez Roura
 *
 */
public class Puntuacion extends Sprite {
	
	int puntos = 0;
	/**
	 * Constructor de la puntuacion
	 * @param x1
	 * @param y1
	 * @param f
	 */
	public Puntuacion(int x1, int y1, Field f) {
		super("Puntuacion", x1, y1, x1, y1, 0, " ", f);//le dices solo el inicio por que al poner el tamaño se autoajustan
		this.solid = false;
		this.text = true;
		this.font = new Font("Arial", font.BOLD, 30);
		this.textColor = 0xFFFFFF;
		addPuntos(0);
	}
	
	/**
	 * Suma puntos al hud
	 * @param puntos
	 */
	public void addPuntos(int puntos) {
		this.puntos += puntos;
		this.path = "Puntos : " + this.puntos;
	}
	
}
